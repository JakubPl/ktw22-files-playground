import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        try {
            writeToFile("ala ma kota");
            Files.write(Paths.get("test.txt"),"ala ma psa".getBytes());
            System.out.println(Files.readString(Paths.get("toRead.txt")));
        } catch (IOException e) {
            System.out.println("Problem z odczytem pliku");
        }
    }


    private static void writeToFile(String toWrite) throws IOException {
        try(FileOutputStream fileOutputStream = new FileOutputStream("test.txt")) {
            fileOutputStream.write(toWrite.getBytes());
        }
    }
}
